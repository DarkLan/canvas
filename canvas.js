"use strict"; 

var canvas = document.getElementById('canvas');

var windowW = window.innerWidth;
var windowH = window.innerHeight;

canvas.height = windowH;
canvas.width = windowW;

var c = canvas.getContext('2d');


function Circle(x, y, r, mx, my) {
	this.x = x;
	this.y = y;
	this.mx = mx;
	this.my = mx;
	this.r = r;
}

Object.prototype.update = function() {
	if (this.x + this.r >= windowW || this.x - this.r <= 0) {
		this.mx = -this.mx;
	}

	if (this.y + this.r >= windowH || this.y - this.r <= 0) {
		this.my = -this.my;
	}
	this.x += this.mx;
	this.y += this.my;

	this.draw();
}

Object.prototype.draw = function() {
		c.beginPath();
		c.arc(this.x, this.y, this.r, 0, Math.PI * 2, false);
		c.fillStyle = 'rgb(255,255,255, 1)';
		c.strokeStyle = 'rgb(255,255,255, 1)';
		c.fill();
		c.stroke();
	}


// Implementation
let objects;

function init () {
	objects = [];

	for (let i = 20 - 1; i >= 0; i--) {
		var r = Math.ceil(Math.random() * 20);
		var x = Math.random() * (windowW - r * 2) + r;
		var y = Math.random() * (windowH - r * 2) + r;
		var mx = (Math.random() - 0.5) * 20;
		var my = (Math.random() - 0.5) * 20;
		objects.push(new Circle(x, y, r, mx, my));
	}
}


function animate () {
	requestAnimationFrame(animate);
	c.clearRect(0, 0, windowW, windowH);

	for (var i = 0; i < objects.length; i++) {
		objects[i].update();
	}
}

init();
animate();